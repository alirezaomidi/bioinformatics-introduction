# 1.3 String Alignment


> #### Some Notes
Considering a long DNA sequence string
* **First** First thing to expect is that the measurements obtained in laboratories are reliable only up to a certain degree.  
* **Second** even the process of typing data into computer storage must be seen as error prone up to a certain degree.
* **Third** nature itself is a source of non-exact matching between e.g. homologous strings in a sense that ***evolution may mutate bases, insert fresh bases, or delete existing bases.***

*First of all* String matching in this case is not only exact coincidence of character counts but leads to sequence alignment.
For example:

```
T T A T G C A T A C C T C A T G G G T A C T

T T A C G C G T A C T C A T G G T A C T T
```
aligns as:
```
T T A T G C A T A C – C – T C A T G G G T A C T
T T A C G C G T A C T C A T G G T A C – T – – T
```

Introduction of a spacing symbol in one of the strings might be interpreted as a deletion of a formerly present character from this string, but also as an insertion of a fresh character into the other string.

Now the *second thing* to notice is to score mutations and differences. One usual way for scoring is to look at valid aligned sequences obtained from biological insight or plausibility and count the following frequencies for each pair (x, y) of characters or spacing symbol:

```
p_{x , y} is frequency of appearance of aligned pair (x, y)
p_{x} is the frequency of appearance of x
```

Quotient `p_{xy}/p_{x}p_{y}` expresses how strongly nature attempts to align x with y, provided that x and y are sufficiently present and is indeed the correct measure of evolutionary preference for an alignment of x with y.


```
sigma (x , y) = log (p_{xy}/ p_x * p_y)
```

The above formula indicates the scoring function. The closer quotient `p_{xy}/p_x*p_y` is to zero, the less probable it becomes to find x aligned with y in regions with x and y being sufficiently present, and the stronger pair (x, y) gets rewarded with a negative value. This is guidance for alignment algorithms to avoid aligning x with y.

The *final question* is **how to compute alignment with maximum score efficiently.**

Using brute force method to find the best alignment is not efficient as it takes exponential time to be performed.

# 1.4 Multiple Alignments

Assume a family of proteins. Then we will discuss the case of Multiple Sequence Alignment.
In this part primary goal is not to obtain maximum similarity between pairs of strings, but to **identify subsections of the proteins that are strongly conserved within all strings**.

Usually  highly conserved subsections are those that define the biological function of the proteins within the family and also remaining sections are those that have **experienced variation during evolution without leading to an extinction** of the species since they are not essential for biological function.

With a fixed scoring function, a multiple alignment may be **scored by summing up all scores of pairwise combinations** of aligned strings; Which is called SP-Score. *There are arguments indicating that SP-scoring is, in some sense, doubtful.* Also finding a multiple alignment with the maximum sp-score is **NP-Hard**.

An alternative view to multiple alignments is to say that a multiple alignment defines some sort of consensus string, that is a **string most similar to all aligned strings**.

A first attempt is to look at an alignment column by column and take for each column the character (or spacing symbol) that has maximum summed score with all characters in the considered column. By deleting spacing symbols we obtain a string with maximum summed alignment score with all strings occurring in the lines of the alignment. Such a most similar string is known in algorithm theory as ***Steiner string***.

The method is equivalent to finding a common ancestor for strings S1, . . . , Sk with maximum similarity to all of the strings.

Assume we had knowledge about various ancestors that occurred during evolution from a common root towards leaves S1, . . . , Sk.

Now it makes sense to ask for an assignment of strings to the four ancestor nodes a, b, c, d in the diagram such that the sum of all optimum alignment scores between any two strings connected by a link is maximized. This problem is called ***phylogenetic alignment problem*** in the bioinformatics literature; in algorithm theory it is known as ***Steiner tree problem***. Which is again **NP-Hard**.

# 2.5 Soft Pattern Matching = Alignment

An alignment of string S with string T is obtained by introducing spacing symbols at certain positions of S and T such that two string `S*` and `T*` and no space of these two is filled with two spacing symbols. Like

```
A C – G A – G T T C – A C T
– C T G G C T – T G G A – T
```

is an alignment of `ACGAGTTCACT` with `CTGGCTTGGAT`.

### 2.5.2 Evaluating Alignments by Scoring Function

Quality of an alignment is measured by a numerical value called the **score** that depends on a scoring function σ. The scoring function for two characters shows how a similarity for two same specific characters are rewarded and for two different specific characters are punished. The score of an alignment is defined as the summation of score of all pairs aligned together.

**The Alignment Problem: Given two strings S and T find an alignment Sstar  and Tstar with maximum score sigma_opt (Sstar , Tstar)**


# 2.6 Multiple Alignments

## 2.6.1 Sum of Pairs Maximization

Given  S1, . . . , Sk, a multiple alignment consists of strings T1, . . . , Tk of the same length that result from S1, . . . , Sk by insertion of spacing symbols at certain positions, with the restriction that columns consisting of spacing symbols only are not allowed.
