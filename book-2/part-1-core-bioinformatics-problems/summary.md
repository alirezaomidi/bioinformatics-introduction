# Chapter 1: Core Bioinformatics Problems

## DNA Mapping and Sequencing

### Size of Human DNA

Human genome consists of approximately **3.3 billion** (i.e. 3.300.000.000) base pairs. These are organized into **23 chromosome** pairs of size ranging up to a few 100 millions of base pairs. Let us make the *naive assumption that we could read out bases of a human genome base by base with a rate of 1 second per base. Working 8 hours a day and 200 days a year it would take us 3.300.000.000/60 × 60 × 8 × 200 = 572 years* to analyze a single human DNA molecule.

Rapid sequencing of long DNA molecules is based on **massive parallel processing** of fragments of DNA

### Methods for Manipulation with DNA in Biology Labs

#### Copying DNA: Polymerase Chain Reaction (PCR)

Working with DNA in laboratories requires lots of copies of the respective molecule. PCR provides production of identical copies of DNA in cheap and fast manner.

DNA splits into its two strands. With the presence of a sufficient amount of bases and the **polymerase enzyme**, *each of the strands is rapidly complemented to a double-stranded copy* of the original molecule. Repeating this process leads to an *exponential* increase in the number of identical copies, 2^n after the nth iteration.

#### Hybridizations and Microarrays

Fix S onto a glass plate, then flood this glass plate with a solution of **fluorescent (or radioactively marked)** copies of T. After a while, wash out whatever has not hybridized to S and detect via fluorescence whether S has hybridized to a copy of T.

The testing works by making a **photo of fluorescent** spots which then is examined by automated methods. As an example, by working with lots of expressed mRNA strings one can obtain a complete snapshot of *what is going on in a cell at a certain moment.*

#### Cutting DNA into fragments

Whenever only random fragments of DNA molecule copies are required, one
may apply ultrasonic or press DNA through spray valves to achieve arbitrary
cutting into fragments.

Also using restriction enzymes is an other clearly controlled way, Each restriction enzyme cuts the DNA at every position where a certain short
pattern, called restriction site, is present  In order
to ensure that both strands are cut, it is required that wherever the restriction
site occurs on one strand, it also has to occur on the other strand at the same
position  it
means that restriction sites must be complementary palindromes.
Assuming that the corresponding restriction enzyme cuts
between the two bases A, the resulting DNA strings possess so-called sticky
ends. In case that cutting is performed in the middle of the restriction sites,
i.e. between the second base A and first base T, separation leaves so-called
blunt ends.

#### Prefix Cutting

A special way of cutting DNA is to produce every initial segment ending with
a fixed base,


#### Unique Markers

There are special DNA strings called **sequence tagged sites (STS)**, known to occur exactly once on chromosomes.

> what's the use case of theme?

#### Measuring DNA Length

#### Sequencing Short DNA Molecules
DNA molecules up to a length of approximately 700 base pairs may be rapidly
and automatically sequenced in the laboratory employing a method called **gel
electrophoresis**.

Levels to do this task:

* Generating Copies of Molecule
* All prefix ending with `A` are constructed
* These prefixes are put onto a gel with an electric
field applied to it
* for each of the remaining bases C, G, and T repeating above processes.
* On the gel we obtain a
pattern as in the example shown

![](short-dna.png)  

* On the gel we obtain a
pattern as in the example shown

#### Mapping Long DNA Molecules
