# Chapter 1: Biological Background

## 1.1 Cell Structure

There are many different types of cells. Cells varies in size, shape and type depending on where it is located and what functions it suppose to do. Now we consider a concept of **generallized cell**. Cells always consists of three parts:

![](/home/soroush/Dropbox/Documents/bioinformatics-introduction/biology-intro/cell_structure.jpg) 

 * **Membrace:** The cell is enclosed by it. Membrance seperates material inside the cell (Named as *Intracellular*) and outside of the cell (*Extracellular*). It also maintans the integrity of cell and controlls passage of materials into and out of the cell.
The cell membrane is a double layer of phospholipid molecules. Proteins in the cell membrane provide structural support, form channels for passage of materials, act as receptor sites, function as carrier molecules, and provide identification markers.

* **Cytoplasm:** is a gel like liquid inside the cell. The liquid place provides a suitable place for *chemical reactions* and also performs *expantion, growth and duplication* of the cell. Many materials (called *Organells*) are placed inside the cytoplasm Within the cytoplasm, materials move by *diffusion*, a physical process that can work only for short distances. Examples of cytoplasmic organelles are *mitochondrion, ribosomes, endoplasmic reticulum, golgi apparatus, and lysosomes*.

* **Nucleus and Nucleolus:** 
The nucleus, formed by **a nuclear membrane around a fluid nucleoplasm**, is the control center of the cell. Threads of **chromatin** in the nucleus contain *deoxyribonucleic acid* (DNA), the genetic material of the cell. 
The *nucleolus* is a dense region of *ribonucleic acid* (RNA) in the nucleus and is the site of *ribosome* formation. The nucleus determines how the cell will function, as well as the basic structure of that cell.

> reference: http://training.seer.cancer.gov/anatomy/cells_tissues_membranes/cells/structure.html

## 1.2 Deoxyribonucleic Acid (DNA) 


It is the molecule that carries most of the genetic instructions of all known living organisms. It is heredaitry material for all organisms such as humo sapiense (human). The DNA is located in nucleus (called nuclear DNA) and also some small parts of DNA can be found at mitochondria (where it is called mitochondrial DNA or mtDNA).  The DNA is like a code made of symbols (here the symbols are chemical bases). These symbols are adenine (A), guanine (G), cytosine (C), and thymine (T). In some viruses cytosine may be replaced with hydrioxy methyl or hydrioxy methyl glucose cytosine. 

Depending on the type of organism the size factor of the DNA can vary. For human it is 3B basepairs. More than 99% of DNA in human are same to each other.

Bases join to each other in the way that T joins with A and C joins with G and create a basepair. A DNA is a sequence of these basepairs. Each base is also attached to a sugar molecule and a phosphate molecule. Together, a base, sugar, and phosphate are called a **nucleotide**. Nucleotides are arranged in two long strands that form a spiral called a **double helix**. 

![](/home/soroush/Dropbox/Documents/bioinformatics-introduction/biology-intro/dnastructure.jpg) 

> references: https://en.wikipedia.org/wiki/DNA , https://ghr.nlm.nih.gov/primer/basics/dna

 
## 1.3 Gene

Gene is a region of DNA made of nucleotides and is the molecular unit of heredity. Gene acts as instructions to make molecules called protein. In human the count of the genes are estimated between 20000 to 25000 and the size of a gene varies between few hundred DNA bases to more than 2 million bases.

![](/home/soroush/Dropbox/Documents/bioinformatics-introduction/biology-intro/geneinchromosome.jpg) 

Every person has two copies of each gene, one inherited from each parent. Most genes are the same in all people, but a small number of genes (less than 1 percent of the total) are slightly different between people. Alleles are forms of the same gene with small differences in their sequence of DNA bases. These small differences contribute to each person’s unique physical features.

> reference: https://ghr.nlm.nih.gov/primer/basics/gene

## 1.4 Genome

The genome is the genetic material of an organism. It consists of DNA (or RNA in RNA viruses). The genome includes both the genes, (the coding regions), the noncoding DNA and the genomes of the mitochondria and chloroplasts.

## 1.5 RNA
Ribonucleic acid (RNA) is a polymeric molecule implicated in various biological roles in coding, decoding, regulation, and expression of genes. 

> references: http://www.rnasociety.org/about/what-is-rna/ , https://en.wikipedia.org/wiki/RNA

## 1.6 Protein

## 1.7 Transcription / Translation

## 1.8 Gene Expresion

Gene expression is the process by which genetic instructions are used to synthesize gene products. These products are usually proteins, which go on to perform essential functions as enzymes, hormones and receptors, for example. Genes that do not code for proteins such as ribosomal RNA or transfer RNA code for functional RNA products.

Several steps in the gene expression process may be modulated, including the transcription, RNA splicing, translation, and post-translational modification of a protein. 

> references: https://en.wikipedia.org/wiki/Gene_expression , http://www.news-medical.net/life-sciences/What-is-Gene-Expression.aspx

## 1.9 DNA Sequencing

Is the process of finding the precise order of nucleotides in DNA molecule; means knowing the precise sequence of A,T,C and G creating DNA. As the applications of Sequencing we can ...: 
 * Determine the sequence of individual Genes
 * Used in molecular biology to study gnomes and proteins they encode.
 * Used in evelutionary biology ti study how different organisms are related and how evolved.
 *  Sequencing enables researchers to determine which types of microbes may be present in a microbiome.
 * Medical technicians may sequence genes from patients to determine if there is risk of genetic diseases.

> The field of **Metagenomics** involves identification of or-
ganisms present in a body of water, sewage, dirt, de-
bris filtered from the air, or swab samples from organ-
isms. 






