# Work Flow of Microarray Data Anaysis

## What is Microarray Technology?

## Programming Work Flow

### BioConductor Package



### Installation of Required Packages

### Getting The Data

Our sample data in this documentation is GSE20986 and the GEO Platform is GPL570 – the GEO short code for the Affymetrix Human Genome U133 Plus 2.0 Array, a commonly used microarray chip for human transcriptome studies.

For each chip, the data table shows the probeset alongside a normalized expression value for the probeset. The normalization method can be found in the table header.

In order to download a sample data from `GEO`, in R language there is a package named `GEOquery`. If not installed on your system you can download and install it via `biocLite` function in `BiocInstaller` package.

NCBI GEO allows supplemental files to be attached to GEO Series (GSE), GEO platforms (GPL), and GEO samples (GSM). To download the file there is a function `getGEOSuppFile("access code")`. Second and third arguments are to ask whether to create a directory to put the files in or not, and for getting the download directory. These two have default values.

```R
library (GEOquery)
getGEOSuppFiles("GSE20986")
```  

Result of following command is a `.tar` compressed file and a text file near that which includes a list of files inside the `.tar` file.

```
#Archive/File   Name    Time    Size    Type
Archive GSE20986_RAW.tar        01/17/2013 14:57:27     56360960        TAR
File    GSM524662.CEL.gz        03/20/2010 17:10:33     4708859 CEL
File    GSM524663.CEL.gz        03/20/2010 17:10:36     4859772 CEL
File    GSM524664.CEL.gz        03/20/2010 17:10:39     4875531 CEL
File    GSM524665.CEL.gz        03/20/2010 17:10:42     4265723 CEL
File    GSM524666.CEL.gz        03/20/2010 17:10:45     4667448 CEL
File    GSM524667.CEL.gz        03/20/2010 17:10:48     4579443 CEL
File    GSM524668.CEL.gz        03/20/2010 17:10:51     4771601 CEL
File    GSM524669.CEL.gz        03/20/2010 17:10:54     4506786 CEL
File    GSM524670.CEL.gz        03/20/2010 17:10:57     4603537 CEL
File    GSM524671.CEL.gz        03/20/2010 17:11:00     4898578 CEL
File    GSM524672.CEL.gz        03/20/2010 17:11:04     4810305 CEL
File    GSM524673.CEL.gz        03/20/2010 17:11:08     4796509 CEL

```

To uncompress the files run:

```R
untar ("GSE20986/GSE20986_RAW.tar" , exdir = "data")
cels <- list.files("data/", pattern = "[gz]")
sapply(paste("data", cels, sep="/"), gunzip)
```

which creates a folder named data and then place the extracted files there. Then unzips the sub compressed files and after the above command you will see a list of `.CEL` files in the data folder.

To load the data into R using simpleaffy we need to create a file to represent this information.

There must be a data file like this near the cell files. We named this file as `phenodata.txt`

```
Name FileName Target
GSM524662.CEL GSM524662.CEL iris
GSM524663.CEL GSM524663.CEL retina
GSM524664.CEL GSM524664.CEL retina
GSM524665.CEL GSM524665.CEL iris
GSM524666.CEL GSM524666.CEL retina
GSM524667.CEL GSM524667.CEL iris
GSM524668.CEL GSM524668.CEL choroid
GSM524669.CEL GSM524669.CEL choroid
GSM524670.CEL GSM524670.CEL choroid
GSM524671.CEL GSM524671.CEL huvec
GSM524672.CEL GSM524672.CEL huvec
GSM524673.CEL GSM524673.CEL huvec
```

To download *SOFT* data format (The Data after preprocesses) you can use `getGEO('access code')` function:

```R
GEOquery::getGEO("GSE20986")
...
File stored at:
/tmp/Rtmp8HagtG/GPL570.soft
$GSE20986_series_matrix.txt.gz
ExpressionSet (storageMode: lockedEnvironment)
assayData: 54675 features, 12 samples
  element names: exprs
protocolData: none
phenoData
  sampleNames: GSM524662 GSM524663 ... GSM524673 (12 total)
  varLabels: title geo_accession ... data_row_count (32 total)
  varMetadata: labelDescription
featureData
  featureNames: 1007_s_at 1053_at ... NA.21421 (54675 total)
  fvarLabels: ID GB_ACC ... Gene Ontology Molecular Function (16 total)
  fvarMetadata: Column Description labelDescription
experimentData: use 'experimentData(object)'
Annotation: GPL570
```

Pros of using `getGEO()`:

1. The data size is extremely smaller (58MB -> 2.3MB)  
2. There is no need to preprocess (Normalization, Quality Control, etc.)

Cons:

* You can't modify its normalization and other preprocessing methods. Sometimes we need to choose the methods by ourselves.


#### affyio Package

Routines for parsing **Affymetrix** data files based upon file format information. Primary focus is on accessing the CEL and CDF file formats.

Following functions are inside this package:

* `check.cdf.type`:
* `get.celfile.dates`:
* `read.cdffile.list`:
* `check.cellfile`: returns the celfile contents in class `list`
* `read.celfile.header`: returns the celfile headers in class `list`. Result is something like

```
$cdfName
[1] "HG-U133_Plus_2"

$`CEL dimensions`
Cols Rows
1164 1164

```

* `read.celfile.probeintensity.matrice`


#### affxparser Package

Package for fast and memory efficient parsing of Affymetrix files (CDF, CEL, CHP, BPMAP, BAR) using Affymetrix' Fusion SDK.
Supports both ASCII and binary-based files are supported.
Currently, there are **only** methods for reading chip definition file (CDF) and a cell intensity file (CEL).

1. `readCelUnits()` - reads one or several Affymetrix CEL file probeset by probeset.
2. `readCel()` - reads an Affymetrix CEL file. by probe.
3. `readCdf()` - reads an Affymetrix CDF file. by probe.
4. `readCdfUnits()` - reads an Affymetrix CDF file unit by unit.
5. `readCdfCellIndices()` - Like readCdfUnits(), but returns cell indices only, which is often
enough to read CEL files unit by unit.
6. `applyCdfGroups()` - Re-arranges a CDF structure.
7. `findCdf()` - Locates an Affymetrix CDF file by chip type.

There are also other functions detailed at [affyxparser reference manual](http://bioconductor.org/packages/release/bioc/manuals/affxparser/man/affxparser.pdf).

```R
library(affxparser)
library('AffymetrixDataTestFiles')
path <- system.file("rawData", package="AffymetrixDataTestFiles")
files <- findFiles(pattern="[.](cel|CEL)$", path=path, recursive=TRUE, firstOnly=FALSE)
files <- grep("FusionSDK_Test3", files, value=TRUE)
files <- grep("Calvin", files, value=TRUE)

names(units)
 [1] "Pae_16SrRNA_s_at"         "Pae_23SrRNA_s_at"          "PA1178_oprH_at"
 [4] "PA1816_dnaQ_at"           "PA3183_zwf_at"             "PA3640_dnaE_at"
 [7] "PA4407_ftsZ_at"           "Pae_16SrRNA_s_st"          "Pae_23SrRNA_s_st"
[10] "PA1178_oprH_st"           "PA1816_dnaQ_st"            "PA3183_zwf_st"
[13] "PA3640_dnaE_st"           "PA4407_ftsZ_st"            "AFFX-Athal-Actin_5_r_at"
[16] "AFFX-Athal-Actin_M_at"    "AFFX-Athal-25SrRNA_s_at"   "AFFX-Athal-5SrRNA_at"
...
```

#### illuminaio Package

Parsing Illumina Microarray Output Files

Tools for parsing Illumina's microarray output files, including IDAT.

```R
library(illuminaio)
library(IlluminaDataTestFiles)
idatFile <- system.file("extdata", "idat",
    "4343238080_A_Grn.idat", package = "IlluminaDataTestFiles")
idat <- readIDAT(idatFile)

names(idat)
[1] "Barcode" "Section" "ChipType" "Quants" "RunInfo"
```

#### MiChip Package

MiChip is a microarray platform using locked oligonucleotides for the analysis of the expression of microRNAs. The MiChip library provides a set of functions for *loading data* from several MiChip hybridizations, *flag correction*, *filtering* and *summarizing* the data. The data is then packaged as a Bioconductor ExpressionSet object where it can easily be further analyzed with the Bioconductor toolset.

```R
library(MiChip)
datadir <- system.file("extdata", package="MiChip")
defaultRawData <- parseRawData(datadir)

ExpressionSet (storageMode: lockedEnvironment)
assayData: 4608 features, 2 samples
  element names: exprs, flags
protocolData: none
...
```


### Loading Normalizing, and Summarizing Data


#### GCRMA Probe Summarization

GCRMA is a method of converting .CEL files into expression set using the Robust Multi-array Average (RMA) with the help of probe sequence and with GC-content background correction.

The background correction  is  motivated  by  the  assumptions  that  observed  PM  and  MM  values  consist  of  optical noise, non-specific binding noise, and signal.

This algorithm performs three jobs:

* Background Correction
* Normalization
* Probe Summarization: Conversion of probe level values to probeset expression values in an outlier resistance manner.

#### simpleaffy Package

```R
library (simpleaffy)
celfiles <- read.affy(covdesc="phenodata.txt", path="data")
celfiles
```
The celfile class is `affyBatch`. This is a class representation for Affymetrix GeneChip probe level data. The main component are the intensities from multiple arrays of the same CDF type.

```R
celfiles.gcrma <- gcrma(celfiles)

```

The above code will perform GCRMA on the cel data. The result stored in celfiles.gcrma is an object of class `ExpressionSet`.

> `ExpressionSet` class is a class to contain high throughput expression level assays.  

#### [gcrma Package](http://bioconductor.org/packages/release/bioc/html/gcrma.html)

Background Adjustment Using Sequence Information.



#### farms Package

The `farms` package provides a new summarization algorithm called **FARMS** - Factor Analysis for Robust Microarray Summarization and a novel unsupervised feature selection criterion called I/NI-calls.

The summarization method is based on a factor analysis model for which a Bayesian Maximum
a Posteriori method optimizes the model parameters under the assumption of Gaussian measurement
noise. Thereafter, the RNA concentration is estimated from the model.

`farms` does not use background correction and uses either *quantile normalization* or *cyclic loess*. Neverthess any other `affy` preprocessing method can be applied as well. It does not apply *PM corrections* and uses PMs only.

```R
library(farms)
library(affydata)
data(Dilution)
eset <- qFarms(Dilution)  # Quickly crete an expression set object
# Or we can use expFarms which accepts more options
eset <- expFarms(Dilution, bgcorrect.method = "rma",
    pmcorrect.method = "pmonly", normalize.method = "constant")
```

`eset` is an object of type `ExpressionSet` which can be analyzed in many *BioConductor* packages.


### Quality Control and Checking
Before we analyse the data it is imperative that we do some quality control checks to make sure there are no issues with the dataset. The first thing we can do is check the **effects of the GC-RMA normalisation**, by plotting a boxplot of probe intensities before and after normalisation.

#### affyPLM Package

Methods for fitting probe-level models.


```R
# load colour libraries
library(RColorBrewer)
# set colour palette
cols <- brewer.pal(8, "Set1")
# plot a boxplot of unnormalised intensity values
boxplot(celfiles, col=cols)
# plot a boxplot of normalised intensity values, affyPLM is required to interrogate celfiles.gcrma
library(affyPLM)
boxplot(celfiles.gcrma, col=cols)
# the boxplots are somewhat skewed by the normalisation algorithm
# and it is often more informative to look at density plots
# Plot a density vs log intensity histogram for the unnormalised data
hist(celfiles, col=cols)
# Plot a density vs log intensity histogram for the normalised data
hist(celfiles.gcrma, col=cols)

```

![](boxplot.png)
![](gcrma-boxplot.png)


![](hist.png)
![](gcrma-hist.png)

From these plots we can conclude that there are no major deviations amongst the 12 chips, and normalisation has brought the intensities from all of the chips into distributions with similar characteristics.


This package also visualizes statistical characteristics of CEL files.

* `celfiles.qc <- fitPLM(celfiles)`: Perform probe-level metric calculations on the CEL files. The result is an object of class PLMset.
> **PLMSet**: This is a class representation for Probe level Linear Models fitted to Affymetrix GeneChip probe level data.

* `image(celfiles.qc, which=1, add.legend=TRUE)`: Create an image of GSM524665.CEL.
 Result:

![](qc-image.png)


* `RLE(celfiles.qc, main="RLE")`: RLE (Relative Log Expression) plots should have values close to zero. GSM524665.CEL is an outlier.

![](rle.png)

* `NUSE(celfiles.qc, main="NUSE")`: We can also use NUSE (Normalized Unscaled Standard Errors). The median standard error should be 1 for most genes.

GSM524665.CEL appears to be an outlier on this plot too

![](nuse.png)

Following commands result hierarchical clustering to find relationship between samples.

```R
eset <- exprs(celfiles.gcrma)
distance <- dist(t(eset),method="maximum")
clusters <- hclust(distance)
plot(clusters)
```
![](hclust.png)

### Filtering Data

The first stage of analysis is to filter out **uninformative data such as control probesets and other internal controls** as well as removing genes with low variance, that would be unlikely to pass statistical tests for differential expression, or are expressed uniformly close to background detection levels. The modifiers to nsFilter below tell nsFilter not to remove probesets without Entrez Gene identifiers, or have duplicated Entrez Gene identifiers.

```R

celfiles.filtered <- nsFilter(celfiles.gcrma, require.entrez=FALSE, remove.dupEntrez=FALSE)

```

In order to find which data was thrown away and why we can run:

```R

celfiles.filtered$filter.log

```

You will see sth like this:

```
$numLowVar
[1] 27307

$feature.exclude
[1] 62
```

This means From this we conclude 27,307 probesets have been removed for reasons of low variance, and 62 control probesets have been removed as well.



#### [affy Package](http://bioconductor.org/packages/release/bioc/html/affy.html)

![affy](http://bioconductor.org/shields/years-in-bioc/affy.svg)

The affy package is for data analysis and exploration of Affymetrix oligonucleotide array probe level data.  
The software utilities provided with the Affymetrix software suite summarizes the probe set intensities to form one expression measure for each gene. The expression measure is the data available for analysis.

Some of the features are:

- Convert probe level data to expression values
    - Read *CEL* file information
    - Expression measures i.e. *RMA*, *MAS 5.0*, *MBEI*
- Quality Control
    - *MA Plot*
    - *PM* Correct
    - *Histograms*, *Images* & *Boxplots*
    - *RNA Degradation* plots
- Normalization


#### [affyQCReport Package](http://bioconductor.org/packages/release/bioc/html/affyQCReport.html)

This package creates a QC report for an AffyBatch object. The report is intended to allow the user to quickly assess the quality of a set of arrays in an AffyBatch object.


### Finding Differentially Expressed Probesets

There are some packages for analyzing microarray data files like `limma` and `affy` that we will describe them below.
To make the data ready for them following processes are required:

```R

samples <- celfiles.gcrma$Target
samples
samples <- as.factor(samples)
# set up the experimental design
design <- model.matrix(~0 + samples)
colnames(design) <- c("choroid", "huvec", "iris", "retina")

```

then the `design` is

```
choroid huvec iris retina
1        0     0    1      0
2        0     0    0      1
3        0     0    0      1
4        0     0    1      0
5        0     0    0      1
6        0     0    1      0
7        1     0    0      0
8        1     0    0      0
9        1     0    0      0
10       0     1    0      0
11       0     1    0      0
12       0     1    0      0
attr(,"assign")
[1] 1 1 1 1
attr(,"contrasts")
attr(,"contrasts")$samples
[1] "contr.treatment"
```

### Analyzing Data

#### limma Package


#### [lumi Package](http://bioconductor.org/packages/release/bioc/html/lumi.html)
integrated solution for the Illumina microarray data analysis. It includes functions of Illumina BeadStudio (GenomeStudio) data input, quality control, BeadArray-specific variance stabilization, normalization and gene annotation at the probe level. It also includes the functions of processing Illumina methylation microarrays

#### [minfi Package](http://bioconductor.org/packages/release/bioc/html/minfi.html)
Analyzing and visualizing Illumina's methylation array data.
