# Chapter 5: Sequence Homology

The **Evolution Theory** tells us that gene sequences may have derived from common ancestral sequences. Comparison of biological sequences in this context is based on counting the **number of mutations, insertions, and deletions** of bases necessary to transform one DNA sequence into another.

> Are the difference of **Silent Parts** of the DNA important in this area?

* **Dot Plot (Similarity Matrix)**: Used to visualize the similarity between to proteins and nucleic acids. It is  based on the *number and length of matching segments* between the sequences.

As it's applications we can perform learning the functional and
structural properties of the new sequence through analogy *based on what is
already known about the established sequence*.

### Information Retrieval from Biological Databases

Two basic methods for searching sequence databases:
* particular keyword label (e.g., “cytochrome c” or “dopamine precursor”)
* search engines can be used to hunt for sequences that are similar to one another – **homology based searches**.

### Entrez

Between more than one keywords following operators can be used:

* `AND`: Both keywords must be found in the search results.
* `OR`: At least one of the keywords must exist in the results.
* `NOT`: the next keyword should not exist in the results.

### Obtaining Sequences with R

The bioconductor software systems provide a package named `genbankr` which transforms GenBank files into semantically useful objects.

To install the package if you have bioconductor installed on your system at first open R-Lang as **sudoer** then write following commands:

To start the bioconductor installer package and install `genbankr`:

```R
library(BiocInstaller)
biocLite("genbankr")
```

Also there is an other package named `ape` that can read directly from GenBank. This package can be installed compilation, The instructions are as following:

> TODO


To read data from GenBank simply write:

```R
library (ape)

data <- read.GenBank('NM003949')

print (data)
```

and the result is:

```
1 DNA sequence in binary format stored in a list.

Sequence length: 4100

Label: NM003949

Base composition:
    a     c     g     t
0.199 0.295 0.303 0.203
```

### Sequence Dot Plot

One of the simple techniques for visualizing the similarity between two nucleotide or protein sequences is dot plot.

When the residues of both sequences match at the same location on the plot, a dot is drawn at the corresponding position – a black dot being placed if the nucleotides or the residues are identical. Thus, matching sequence segments appear as runs of diagonal lines across the matrix. An estimation of similarity of the two sequences is gleaned from the length, and the count of matching segments shown in the dot-plot matrix.


To enhance selectivity of matches reported by dot-plots, and to reduce the noise attributed to matching sequence segments arising simply by chance, a threshold tuple-size is sometimes utilized. For example, a match based on tuple-size of 3 requires three residues or nucleotides to match between the sequence along the x-axis and the sequence along the y-axis before a dot is placed on the matrix. This is effective because the chance matches go down with a tuple size of 3.

taking the reverse complement of DNA sequence to determine if the matching strands are complementary to each other.

For drawing the sequence dot plot in R-Lang the package `seqinr` must be installed.

```R
install.packages("seqinr")

library(seqinr)


seq1 <- 'ACCTGACTGGGCTTAGACCTTGAACTTGAACACTTGCCTT'
seq2 <- 'TTTTTACCTGACTGGGCTTGGGGGAGACCTTGAACTTGAACACTTGCCTT'

dotplot (seq1 , seq2)
```

Following code will face with error because `seq1` and `seq2` are in the format of string (or `"character"`) but the function will accept only vector of single characters.

By default `read.GenBank` doesn't include sequence, using the argument `as.character = TRUE` then the sequence is available and also the result data type is list instead of `DNAbin`

```R

gendata1 <- read.GenBank('AB060288' , as.character = TRUE)

```  

then the result is

```
$AB060288
  [1] "a" "t" "g" "g" "t" "g" "a" "a" "a" "a" "g" "c" "c" "a" "c" "a" "t" "a" "g"
 [20] "g" "c" "a" "g" "t" "t" "g" "g" "a" "t" "c" "c" "t" "g" "g" "t" "t" "c" "t"
 [39] "c" "t" "t" "t" "g" "t" "g" "g" "c" "c" "a" "t" "g" "t" "g" "g" "a" "g" "t"
 [58] "g" "a" "c" "g" "t" "g" "g" "g" "c" "c" "t" "c" "t" "g" "c" "a" "a" "g" "a"
 [77] "a" "g" "c" "g" "a" "c" "c" "a" "a" "a" "a" "c" "c" "t" "g" "g" "c" "g" "g"
 [96] "a" "g" "g" "a" "t" "g" "g" "a" "a" "c" "a" "c" "t" "g" "g" "g" "g" "g" "g"
[115] "a" "g" "c" "c" "g" "a" "t" "a" "c" "c" "c" "g" "g" "g" "a" "c" "a" "g" "g"
[134] "g" "c" "a" "g" "t" "c" "c" "t" "g" "g" "a" "g" "g" "c" "a" "a" "c" "c" "g"
[153] "c" "t" "a" "t" "c" "c" "a" "c" "c" "t" "c" "a" "g" "g" "g" "a" "g" "g" "g"
[172] "g" "g" "t" "g" "g" "c" "t" "g" "g" "g" "g" "t" "c" "a" "g" "c" "c" "c" "c"
[191] "a" "t" "g" "g" "a" "g" "g" "t" "g" "g" "c" "t" "g" "g" "g" "g" "c" "c" "a"
[210] "a" "c" "c" "t" "c" "a" "t" "g" "g" "a" "g" "g" "t" "g" "g" "c" "t" "g" "g"
[229] "g" "g" "t" "c" "a" "g" "c" "c" "c" "c" "a" "t" "g" "g" "t" "g" "g" "t" "g"
[248] "g" "c" "t" "g" "g" "g" "g" "a" "c" "a" "g" "c" "c" "a" "c" "a" "t" "g" "g"
[267] "t" "g" "g" "t" "g" "g" "a" "g" "g" "c" "t" "g" "g" "g" "g" "t" "c" "a" "a"
[286] "g" "g" "t" "g" "g" "t" "a" "g" "c" "c" "a" "c" "a" "g" "t" "c" "a" "g" "t"
[305] "g" "g" "a" "a" "c" "a" "a" "g" "c" "c" "c" "a" "g" "t" "a" "a" "g" "c" "c"
[324] "a" "a" "a" "a" "a" "c" "c" "a" "a" "c" "a" "t" "g" "a" "a" "g" "c" "a" "t"
[343] "g" "t" "g" "g" "c" "a" "g" "g" "a" "g" "c" "t" "g" "c" "t" "g" "c" "a" "g"
[362] "c" "t" "g" "g" "a" "g" "c" "a" "g" "t" "g" "g" "t" "a" "g" "g" "g" "g" "g"
[381] "c" "c" "t" "t" "g" "g" "t" "g" "g" "c" "t" "a" "c" "a" "t" "g" "c" "t" "g"
[400] "g" "g" "a" "a" "g" "t" "g" "c" "c" "a" "t" "g" "a" "g" "c" "a" "g" "g" "c"
[419] "c" "t" "c" "t" "t" "a" "t" "a" "c" "a" "t" "t" "t" "t" "g" "g" "c" "a" "a"
[438] "t" "g" "a" "c" "t" "a" "t" "g" "a" "g" "g" "a" "c" "c" "g" "t" "t" "a" "c"
[457] "t" "a" "t" "c" "g" "t" "g" "a" "a" "a" "a" "c" "a" "t" "g" "t" "a" "c" "c"
[476] "g" "t" "t" "a" "c" "c" "c" "c" "a" "a" "c" "c" "a" "a" "g" "t" "g" "t" "a"
[495] "c" "t" "a" "c" "a" "g" "a" "c" "c" "a" "g" "t" "g" "g" "a" "t" "c" "a" "g"
[514] "t" "a" "t" "a" "g" "t" "a" "a" "c" "c" "a" "g" "a" "a" "c" "a" "a" "c" "t"
[533] "t" "t" "g" "t" "g" "c" "a" "t" "g" "a" "c" "t" "g" "t" "g" "t" "c" "a" "a"
[552] "c" "a" "t" "c" "a" "c" "a" "g" "t" "c" "a" "a" "g" "c" "a" "a" "c" "a" "c"
[571] "a" "c" "a" "g" "t" "c" "a" "c" "c" "a" "c" "c" "a" "c" "c" "a" "c" "c" "a"
[590] "a" "g" "g" "g" "g" "g" "a" "g" "a" "a" "c" "t" "t" "c" "a" "c" "c" "g" "a"
[609] "a" "a" "c" "t" "g" "a" "c" "a" "t" "c" "a" "a" "g" "a" "t" "a" "a" "t" "g"
[628] "g" "a" "g" "c" "g" "a" "g" "t" "g" "g" "t" "g" "g" "a" "g" "c" "a" "a" "a"
[647] "t" "g" "t" "g" "c" "a" "t" "c" "a" "c" "c" "c" "a" "g" "t" "a" "c" "c" "a"
[666] "g" "a" "g" "a" "g" "a" "a" "t" "c" "c" "c" "a" "g" "g" "c" "t" "t" "a" "t"
[685] "t" "a" "c" "c" "a" "a" "a" "g" "g" "g" "g" "g" "g" "c" "a" "a" "g" "t" "g"
[704] "t" "g" "a" "t" "c" "c" "t" "c" "t" "t" "t" "t" "c" "t" "t" "c" "c" "c" "c"
[723] "t" "c" "c" "t" "g" "t" "g" "a" "t" "c" "c" "t" "c" "c" "t" "c" "a" "t" "c"
[742] "t" "c" "t" "t" "t" "c" "c" "t" "c" "a" "t" "t" "t" "t" "t" "c" "t" "c" "a"
[761] "t" "a" "g" "t" "a" "g" "g" "a" "t" "a" "g"

attr(,"species")
[1] "Ovis_aries_musimon"
```

Following code transforms the data to a `character` type.
```R
dna1 <- as.vector(gendata1[[1]])
dna2 <- as.vector(gendata2[[1]])
```

now running `dotplot(dna1 , dna2)` results the dot plot we discussed!

```R

dotPlot(dna1 , dna2 ,wsize = 3 ,  nmatch = 3)

```

### Sequence Alignment

This operation involves placing one sequence above the other and and comparing the aligned vertical pairs to establish the correspondence between the sequences at each position.

The matching pair in the two sequences is shown as a (—). The (:) is often used to identify similar but nonidentical pairs. The IUPAC ambiguity code N s used which pairs with G, C, T, or A.



```
10 AANCGTGATCGATGC 25
   ||:||| ||||||||
20 AATCGTTATCGATGC 35

```

### Dynamic Programming and Sequence Alignment Metrics

#### Distance Based Alignment

The algorithm is optimization based minimization of distance between two sequences  demonstrates the computation of Global Alignment using **Needleman-Wunsch** global alignment algorithm.

```
                [D(i - 1 , j - 1) + d(ai , bj)
D(i , j) = min  [D (i - 1 , j) + d(- , bj)     
                [D (i , j - 1) + + d(ai , -)
```


where


```
            [0 if a = b
d(a , b) =  [1 o.w.

```

Each cell shows the possible optimal predecessors that an alignment path may take along any of the multiple optimal paths. Note that the optimal alignment is not identical.

#### Similarity Based alignment

This algorithm calculates a maximization problem and  shows the possible predecessors that are its optimal predecessors in that an alignment path may take any of the multiply optimal paths.


```
                [S(i - 1 , j - 1) + s(ai , bj)
S(i , j) = max  [S (i - 1 , j) + s(- , bj)     
                [S (i , j - 1) + + s(ai , -)
```


where

```
            [1 if a = b
s(a , b) =  [-1 o.w.

```


#### Longest Common Subsequence

the scoring matrix is chosen such that the mismatches have a score of zero while the matches account for a score of +1

![](lcs.png)

#### Insertion, Deletion and Substitution Operations

For computing similarity scores generally assigns 0 to a match, some negative
number to a mismatch and a larger negative number to an insertion and deletion, or indel, events.


### R Packages about Sequence Alignment

##### seqLogo

![](http://bioconductor.org/shields/downloads/seqLogo.svg)

An alignment of DNA or amino acid sequences is commonly represented in the form of a position weight matrix (PWM), a J . W matrix in which position ( j, w ) gives the probability of observing nucleotide j in position w of an alignment of length W. This matrix is stored in a `pwm` class in R. Also it provides a function to plot sequence logo diagram.

![](seqlogo.png)

##### Rsubread
![](http://bioconductor.org/shields/downloads/Rsubread.svg)

This package provides facilities for processing the read data generated by next-gen sequencing technologies including quality assessment, read alignment, read summarization, exon-exon junction detection, absolute expression calling and SNP discovery.

They can be used to analyze data generated from all major sequencing platforms including Illumina GA/HiSeq, Roche 454, ABI SOLiD and Ion Torrent.

##### muscle
![](http://bioconductor.org/shields/downloads/muscle.svg)

Provides multiple sequence alignments of nucleotide or amino acid sequences. This package is in BioConductor project so can be installed via `biocLite` function in `BiocInstaller` package.

There is a sample data inside the package that can be reachable as a variable named `umax` and the class of this variable which is an argument for the `muscle` function is `DNAStringSet`. The function receives a `DNAStringSet` and performs the multiple sequence alignment for them and the result is in the form of a class named `DNAMultipleAlignment`.

To manipulate your own data stored on a file you can use one of the DNAStringSet read function `readDNAStringSet`, `readRNAStringSet`, or `readAAStringSet`.



##### msa

One of the most general packages for multiple sequence alignment is `msa` package reachable from BioConductor. Running following command on a data with type same as the `muscle` package will result a multiple sequence alignment. This package also supports processing multiple alignments and can integrate with other packages.

```R

myFirstAlignment <- msa(mySequences)

```

This package also provides a print function for latex, Named `msaPrettyPrint`.

The msa function also receives an argument to determine the algorithm used to reach msa result.


* This package also can work with OpenMP to provide algorithms in multi-core systems.
* One of the known issues for this package is **memory leak**.
